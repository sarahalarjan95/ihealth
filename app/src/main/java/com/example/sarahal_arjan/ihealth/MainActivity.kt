package com.example.sarahal_arjan.ihealth

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDoctor.setOnClickListener {
            var intent= Intent(this,DoctorActivity::class.java)
            startActivity(intent)
        }
        mPatient.setOnClickListener {
            var intent= Intent(this,Patient::class.java)
            startActivity(intent)

        }

    }
}
