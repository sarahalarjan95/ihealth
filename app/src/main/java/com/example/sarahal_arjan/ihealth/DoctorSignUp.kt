package com.example.sarahal_arjan.ihealth

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_doctor_sign_up.*

class DoctorSignUp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_sign_up)
        mDocSignUp.setOnClickListener {
            var intent= Intent(this,DoctorHomePage::class.java)
            startActivity(intent)
        }
    }
}
